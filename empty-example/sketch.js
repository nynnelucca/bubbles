//7.4: Mouse Interaction with Objects - p5.js Tutorial: https://www.youtube.com/watch?v=TaN5At5RWH8&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=31

let bubbles = [];

function setup() {
  createCanvas(600, 500);
  for (let i = 0; i < 10; i++) {
    let x = random(width);
    let y = random(height);
    let r = random(10, 30);
    let b = new Bubble(x, y, r); //referred to again in the constructor function with this.x, this.y, this.r
    bubbles.push(b); //adds the bubble to the end of the array
  //   bubbles[i] = new Bubble(x, y, r);
  }
}

// function mouseDragged() { //can also use mousePressed()
//   let r = random (10, 50);
//   let b = new Bubble(mouseX, mouseY, r);
//   bubbles.push(b); //adds the bubble to the end of the array
// }

/*I know globally whenever user clicks the mouse so I can create another function locally within the Bubble class, ccf. 'clicked()'.
This means that the function lets me know if the mouse has been pressed ANYWHERE, then it's up to me to determine whether it has been pressed on bubble*/
function mousePressed (){
  for (let i = 0; i < bubbles.length; i++) { //what this is saying -> for every bubble, in the bubbles array use this if the counter plays a role
    bubbles[i].clicked(mouseX, mouseY);
  }
}

function draw() {
  background(0);
/*exactly the same as below, give me every single bubble in order in the array bubbles (cf. "16.4 for...of loops").
'bubble' is the word THAT STANDS IN for every element of the array*/
  // for (let bubble of bubbles) {
  //   bubble.move();
  //   bubble.show();
  // }

  for (let i = 0; i < bubbles.length; i++) { //what this is saying -> for every bubble, in the bubbles array use this if the counter plays a role
    bubbles[i].move();
    bubbles[i].show();
  }
}

class Bubble { //encapsulation of everything it means to be a certain thing using a 'class' -> can also be thought of as a template / blueprint / COOKIECUTTER
  constructor(tempX, tempY, tempR) { //variables inside constructor = temporary local variables. constructor = a  function that contains data of potential object instance
    this.x = tempX; //data of object
    this.y = tempY;
    this.r = tempR;
    this.brightness = 0;
  }

  clicked(x, y) {
    let d = dist(x, y, this.x, this.y); //let me find distance btw. mouseX, mouseY, this.x and this.y
    if (d < this.r) { //then I can say, if d is less than this.r (circles radius), then execute
      console.log("clicked");
      this.brightness = 255;
    }
  }

  move() { //a function that contains functionality of potential object instance
    this.x = this.x + random (-5, 5);
    this.y = this.y + random (-5, 5);
  }

  show() {
    stroke(255);
    strokeWeight(4);
    fill(this.brightness, 100);
    ellipse(this.x, this.y, this.r*2); //uses constructor parameters
  }
}
